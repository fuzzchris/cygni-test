# Instruktioner för backend test - Sten, sax, påse



## Applikationen är deployad på https://cygni-test.herokuapp.com (notera att det är en "free-dyno" så första requestet kan ta upp till 30 sekunder innan ni får svar)

####Spelet körs lättast via Postman

## Endpoints

Följande endpoints finns:

POST /api/games

Startar ett nytt game

```
HTTP/1.1 200 OK
Content-Type: application/json

{
	"name": "John Doe"
}
```

POST /api/games/{id}/join

Tillåter att en andra användare ansluter till spelet

###Request

```
HTTP/1.1 200 OK
Content-Type: application/json
{
	"name": "John Doe"
}
```

POST /api/games/{id}/move

En användare gör ett val

### Request

```
HTTP/1.1 200 OK
Content-Type: application/json
{
	"name": "John Doe",
	"move": "Paper"
}
```

GET /api/games/{id}

Hämtar ett game för specifikt id

### Response

```
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "OK",
    "message": null,
    "data": {
        "id": "1",
        "playerOne": {
            "name": "Test"
        },
        "playerTwo": {
            "name": "Test1"
        },
        "outcome": "Test1 won the game!",
        "status": "FINISHED",
        "moves": [
            {
                "name": "Test1",
                "move": "Paper"
            },
            {
                "name": "Test",
                "move": "Rock"
            }
        ]
    }
}
```



GET /api/history

Hämtar alla avgjorda spel som finns i cachen

### Response

```
HTTP/1.1 200 OK
Content-Type: application/json
{
    "status": "OK",
    "message": "All finished games",
    "data": [
        {
            "id": "1",
            "playerOne": {
                "name": "John"
            },
            "playerTwo": {
                "name": "Doe"
            },
            "outcome": "DRAW",
            "status": "FINISHED",
            "moves": [
                {
                    "name": "Doe",
                    "move": "Rock"
                },
                {
                    "name": "John",
                    "move": "Rock"
                }
            ]
        },
        {
            "id": "2",
            "playerOne": {
                "name": "Test"
            },
            "playerTwo": {
                "name": "Test2"
            },
            "outcome": "Test2 won the game!",
            "status": "FINISHED",
            "moves": [
                {
                    "name": "Test",
                    "move": "Rock"
                },
                {
                    "name": "Test2",
                    "move": "Paper"
                }
            ]
        }
    ]
}
```

