package com.example.cygni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CygniApplication {

    public static void main(String[] args) {
        SpringApplication.run(CygniApplication.class, args);
    }

}
