package com.example.cygni.controllers;

import com.example.cygni.models.Game;
import com.example.cygni.models.GameError;
import com.example.cygni.models.GameStatus;
import com.example.cygni.models.GenericResponse;
import com.example.cygni.models.Move;
import com.example.cygni.models.Player;
import com.example.cygni.utils.CacheService;
import com.example.cygni.utils.GameService;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api")
public class GameController {

    private CacheService<Game> cacheService;
    private GameService gameService;

    public GameController(@NonNull CacheService<Game> cacheService, @NonNull GameService gameService) {
        this.cacheService = cacheService;
        this.gameService = gameService;
    }

    @PostMapping("/games")
    @ResponseBody
    public GenericResponse newGame(@RequestBody Player player) {
        Game newGame = new Game();
        GenericResponse response = new GenericResponse();

        if (StringUtils.isEmpty(player.getName())) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.PLAYER_NAME_EMPTY.toString());
        } else {
            String gameId = cacheService.createGameId();
            newGame.setId(gameId);
            newGame.setPlayerOne(player);
            newGame.setStatus(GameStatus.PENDING);
            cacheService.put(gameId, newGame);
            response.setStatus(HttpStatus.OK);
            response.setMessage(String.format("%s%s", "Id: ", gameId));
        }

        return response;
    }

    @PostMapping("/games/{id}/join")
    @ResponseBody
    public GenericResponse join(@PathVariable("id") String id, @RequestBody Player player) {
        Game game = cacheService.get(id);
        GenericResponse response = new GenericResponse();
        if (game == null) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.GAME_NOT_EXISTING.toString());
        } else if (player.getName().equalsIgnoreCase(game.getPlayerOne().getName())) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.DUPLICATE_PLAYER.toString());
        } else if (game.getStatus().equals(GameStatus.ONGOING)) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.GAME_ALREADY_IN_PLAY.toString());
        } else if (game.getStatus().equals(GameStatus.FINISHED)) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.GAME_IS_OVER.toString());
        } else if (StringUtils.isEmpty(player.getName())) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.PLAYER_NAME_EMPTY.toString());
        } else {
            game.setPlayerTwo(player);
            game.setStatus(GameStatus.ONGOING);

            response.setStatus(HttpStatus.OK);
            response.setMessage("Game on!");
        }

        return response;
    }

    @PostMapping("/games/{id}/move")
    @ResponseBody
    public GenericResponse move(@PathVariable("id") String id, @RequestBody Move move) {
        Game game = cacheService.get(id);
        GenericResponse response = new GenericResponse();

        if (game == null) {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage(GameError.GAME_NOT_EXISTING.toString());
        } else if (game.getStatus().equals(GameStatus.FINISHED)) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.GAME_IS_OVER.toString());
        } else if (game.getPlayerOne() == null || game.getPlayerTwo() == null) {
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            response.setMessage(GameError.NOT_ENOUGH_PLAYERS.toString());
        } else {
            boolean isLegalMove = Move.MOVES.stream()
                    .anyMatch(legalMoves -> legalMoves.equalsIgnoreCase(move.getMove()));

            if (isLegalMove) {
                response.setStatus(HttpStatus.OK);
                response.setMessage(gameService.getOutcome(game, move));
            } else {
                response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                response.setMessage(GameError.ILLEGAL_MOVE.toString());
            }
        }

        return response;
    }

    @GetMapping("/games/{id}")
    @ResponseBody
    public GenericResponse<Game> getGame(@PathVariable("id") String id) {
        Game game = cacheService.get(id);
        GenericResponse<Game> response = new GenericResponse<>();
        if (game == null) {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage("No game with that ID exists");
        } else {
            response.setStatus(HttpStatus.OK);
            // If game is on going, don't show the moves
            if (game.getStatus().equals(GameStatus.ONGOING)) {
                Game onGoingGame = new Game();
                onGoingGame.setPlayerOne(game.getPlayerOne());
                onGoingGame.setPlayerTwo(game.getPlayerTwo());
                onGoingGame.setStatus(game.getStatus());
                onGoingGame.setId(game.getId());
                response.setData(onGoingGame);
            } else {
                response.setData(game);
            }
        }

        return response;
    }

    @GetMapping("/games/history")
    @ResponseBody
    public GenericResponse<List<Game>> getAll() {
        Collection<Game> allGames = cacheService.getAll();
        GenericResponse<List<Game>> response = new GenericResponse<>();
        response.setStatus(HttpStatus.OK);
        response.setMessage("All finished games");
        response.setData(allGames.stream()
                .filter(game -> game.getStatus().equals(GameStatus.FINISHED))
                .collect(Collectors.toList())
        );

        return response;
    }
}
