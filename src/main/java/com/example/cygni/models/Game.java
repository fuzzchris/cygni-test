package com.example.cygni.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Game {
    private String id;
    private Player playerOne;
    private Player playerTwo;
    private String outcome;
    private GameStatus status;
    private List<Move> moves = new ArrayList<>();
}
