package com.example.cygni.models;

public enum GameStatus {
    PENDING,
    ONGOING,
    FINISHED,
    DRAW,
}
