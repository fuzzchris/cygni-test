package com.example.cygni.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class GenericResponse<T> {
    private HttpStatus status;
    private String message;
    private T data;
}
