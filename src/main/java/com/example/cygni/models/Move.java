package com.example.cygni.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Move {
    public static final String ROCK = "rock";
    public static final String PAPER = "paper";
    public static final String SCISSOR = "scissor";
    public static final List<String> MOVES = List.of(ROCK, PAPER, SCISSOR);

    private String name;
    private String move;
}
