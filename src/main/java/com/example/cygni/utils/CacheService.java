package com.example.cygni.utils;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class CacheService<T> {
    private Map<String, T> cache = new HashMap<>();
    private int gameId = 0;

    public T get(String key) {
        return cache.get(key);
    }

    public void put(String key, T data) {
        // If data exists, we delete and re-set it with new object
        T cached = get(key);
        if (cached != null) {
            delete(key);
        }
        cache.put(key, data);
    }

    public void delete(String key) {
        cache.remove(key);
    }

    public Collection<T> getAll() {
        return cache.values();
    }

    public String createGameId() {
        return String.valueOf(++gameId);
    }
}
