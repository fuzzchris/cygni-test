package com.example.cygni.utils;

import com.example.cygni.models.Game;
import com.example.cygni.models.GameError;
import com.example.cygni.models.GameStatus;
import com.example.cygni.models.Move;
import com.example.cygni.models.Player;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GameService {
    public String getOutcome(Game game, Move move) {
        Player player = getPlayer(game, move.getName());

        if (player == null) {
            return GameError.PLAYER_NOT_IN_GAME.toString();
        }

        boolean moveAlreadyMade = game.getMoves().stream()
                .anyMatch(m -> m.getName().equalsIgnoreCase(player.getName()));

        if (moveAlreadyMade) {
            return GameError.MOVE_ALREADY_MADE.toString();
        }

        game.getMoves().add(move);

        return getGameState(game);
    }

    private String getGameState(Game game) {
        if (game.getMoves().size() < 2) {
            return "Waiting for opponent";
        }
        game.setStatus(GameStatus.FINISHED);
        String outcome = getOutcome(game.getMoves());
        game.setOutcome(outcome);
        return outcome;
    }

    private String getOutcome(List<Move> moves) {
        Move playerOneMove = moves.get(0);
        Move playerTwoMove = moves.get(1);

        if (playerOneMove.getMove().equalsIgnoreCase(playerTwoMove.getMove())) {
            return GameStatus.DRAW.toString();
        }

        switch (playerOneMove.getMove().toLowerCase()) {
            case Move.PAPER:
                return playerTwoMove.getMove().equalsIgnoreCase(Move.ROCK) ?
                        formatWinner(playerOneMove.getName()) : formatWinner(playerTwoMove.getName());
            case Move.ROCK:
                return playerTwoMove.getMove().equalsIgnoreCase(Move.SCISSOR) ?
                        formatWinner(playerOneMove.getName()) : formatWinner(playerTwoMove.getName());
            case Move.SCISSOR:
                return playerTwoMove.getMove().equalsIgnoreCase(Move.PAPER) ?
                        formatWinner(playerOneMove.getName()) : formatWinner(playerTwoMove.getName());
        }

        return null;
    }

    private String formatWinner(String winner) {
        return String.format("%s won the game!", winner);
    }

    private Player getPlayer(Game game, String name) {
        Player playerOne = game.getPlayerOne();
        Player playerTwo = game.getPlayerTwo();

        if (playerOne != null && playerOne.getName().equalsIgnoreCase(name)) {
            return playerOne;
        } else if (playerTwo != null && playerTwo.getName().equalsIgnoreCase(name)) {
            return playerTwo;
        }

        return null;
    }
}
