package com.example.cygni.utils;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CacheServiceTest {
    private CacheService<String> cacheService;

    @BeforeAll
    public void setUp() {
        cacheService = new CacheService<>();
    }

    @Test
    public void testGet() {
        cacheService.put("1", "test");
        String test = cacheService.get("1");
        Assertions.assertEquals("test", test);
    }

    @Test
    public void testPut() {
        cacheService.put("1", "test");
        cacheService.put("1", "test2");
        String test = cacheService.get("1");
        Assertions.assertEquals("test2", test);
    }

    @Test
    public void testDelete() {
        cacheService.put("1", "test");
        cacheService.delete("1");
        String test = cacheService.get("1");
        Assertions.assertNull(test);
    }

    @Test
    public void testGetAll() {
        cacheService.put("1", "test");
        cacheService.put("2", "test");
        cacheService.put("3", "test");
        Collection<String> values = cacheService.getAll();
        Assertions.assertEquals(3, values.size());
    }

    @Test
    public void testCreateGameId() {
        String id = cacheService.createGameId();
        Assertions.assertEquals("1", id);
        id = cacheService.createGameId();
        Assertions.assertEquals("2", id);
        id = cacheService.createGameId();
        Assertions.assertEquals("3", id);
    }
}
