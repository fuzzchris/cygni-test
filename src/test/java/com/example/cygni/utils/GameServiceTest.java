package com.example.cygni.utils;

import com.example.cygni.models.Game;
import com.example.cygni.models.GameError;
import com.example.cygni.models.GameStatus;
import com.example.cygni.models.Move;
import com.example.cygni.models.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GameServiceTest {
    GameService gameService;

    @BeforeAll
    public void setUp() {
        gameService = new GameService();
    }

    @Test
    public void getOutcomeWrongPlayer() {
        Game game = new Game();
        game.setPlayerOne(getPlayer("Test"));
        game.setPlayerTwo(getPlayer("Test2"));
        Move move = new Move();
        move.setName("not_in_game");
        String outcome = gameService.getOutcome(game, move);
        Assertions.assertEquals(GameError.PLAYER_NOT_IN_GAME.toString(), outcome);
    }

    @Test
    public void getOutcomeMoveAlreadyMade() {
        Game game = new Game();
        game.setPlayerOne(getPlayer("Test"));
        game.setPlayerTwo(getPlayer("Test2"));
        Move move = new Move();
        move.setName("Test");
        move.setMove("Scissor");
        game.setMoves(List.of(move));

        String outcome = gameService.getOutcome(game, move);
        Assertions.assertEquals(GameError.MOVE_ALREADY_MADE.toString(), outcome);
    }

    @Test
    public void getOutcomeWaitingForOpponent() {
        Game game = new Game();
        game.setPlayerOne(getPlayer("Test"));
        game.setPlayerTwo(getPlayer("Test2"));
        Move move = new Move();
        move.setName("Test");
        move.setMove("Scissor");

        String outcome = gameService.getOutcome(game, move);
        Assertions.assertEquals("Waiting for opponent", outcome);
    }

    @Test
    public void getOutcomeDraw() {
        Game game = new Game();
        game.setPlayerOne(getPlayer("Test"));
        game.setPlayerTwo(getPlayer("Test2"));
        Move move = new Move();
        move.setName("Test");
        move.setMove("Scissor");
        Move move1 = new Move();
        move1.setName("Test2");
        move1.setMove("Scissor");
        game.getMoves().add(move);

        String outcome = gameService.getOutcome(game, move1);
        Assertions.assertEquals(GameStatus.DRAW.toString(), outcome);
    }

    @Test
    public void getOutcomeP1Won() {
        Game game = new Game();
        game.setPlayerOne(getPlayer("Test"));
        game.setPlayerTwo(getPlayer("Test2"));
        Move move = new Move();
        move.setName("Test");
        move.setMove("Scissor");
        Move move1 = new Move();
        move1.setName("Test2");
        move1.setMove("Paper");
        game.getMoves().add(move);

        String outcome = gameService.getOutcome(game, move1);
        Assertions.assertEquals("Test won the game!", outcome);
    }

    @Test
    public void getOutcomeP2Won() {
        Game game = new Game();
        game.setPlayerOne(getPlayer("Test"));
        game.setPlayerTwo(getPlayer("Test2"));
        Move move = new Move();
        move.setName("Test");
        move.setMove("Scissor");
        Move move1 = new Move();
        move1.setName("Test2");
        move1.setMove("Rock");
        game.getMoves().add(move);

        String outcome = gameService.getOutcome(game, move1);
        Assertions.assertEquals("Test2 won the game!", outcome);
    }

    private Player getPlayer(String name) {
        Player p = new Player();
        p.setName(name);
        return p;
    }
}
